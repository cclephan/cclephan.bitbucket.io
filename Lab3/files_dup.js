var files_dup =
[
    [ "encoder2.py", "encoder2_8py.html", "encoder2_8py" ],
    [ "main3.py", "main3_8py.html", "main3_8py" ],
    [ "motor.py", "motor_8py.html", [
      [ "motor.DRV8847", "classmotor_1_1DRV8847.html", "classmotor_1_1DRV8847" ],
      [ "motor.Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.ShareMotorInfo", "classshares_1_1ShareMotorInfo.html", "classshares_1_1ShareMotorInfo" ],
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_encoder_v2.py", "task__encoder__v2_8py.html", "task__encoder__v2_8py" ],
    [ "task_motor.py", "task__motor_8py.html", "task__motor_8py" ]
];