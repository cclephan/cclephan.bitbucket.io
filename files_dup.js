var files_dup =
[
    [ "closedloop.py", "closedloop_8py.html", [
      [ "closedloop.ClosedLoop", "classclosedloop_1_1ClosedLoop.html", "classclosedloop_1_1ClosedLoop" ]
    ] ],
    [ "encoder2.py", "encoder2_8py.html", "encoder2_8py" ],
    [ "IMU.py", "IMU_8py.html", "IMU_8py" ],
    [ "Lab1.py", "Lab1_8py.html", "Lab1_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "main3.py", "main3_8py.html", "main3_8py" ],
    [ "Menu.py", "Menu_8py.html", null ],
    [ "motor.py", "motor_8py.html", [
      [ "motor.DRV8847", "classmotor_1_1DRV8847.html", "classmotor_1_1DRV8847" ],
      [ "motor.Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.ShareMotorInfo", "classshares_1_1ShareMotorInfo.html", "classshares_1_1ShareMotorInfo" ],
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_control.py", "task__control_8py.html", [
      [ "task_control.Task_Controller", "classtask__control_1_1Task__Controller.html", "classtask__control_1_1Task__Controller" ]
    ] ],
    [ "task_data.py", "task__data_8py.html", "task__data_8py" ],
    [ "task_hardware.py", "task__hardware_8py.html", "task__hardware_8py" ],
    [ "Task_IMU.py", "Task__IMU_8py.html", "Task__IMU_8py" ],
    [ "task_motor.py", "task__motor_8py.html", "task__motor_8py" ],
    [ "task_User.py", "task__User_8py.html", "task__User_8py" ],
    [ "tp.py", "tp_8py.html", [
      [ "tp.TouchPanel", "classtp_1_1TouchPanel.html", "classtp_1_1TouchPanel" ]
    ] ]
];