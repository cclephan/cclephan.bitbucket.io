var annotated_dup =
[
    [ "closedloop", null, [
      [ "ClosedLoop", "classclosedloop_1_1ClosedLoop.html", "classclosedloop_1_1ClosedLoop" ]
    ] ],
    [ "encoder2", null, [
      [ "Encoder", "classencoder2_1_1Encoder.html", "classencoder2_1_1Encoder" ]
    ] ],
    [ "IMU", null, [
      [ "BNO055", "classIMU_1_1BNO055.html", "classIMU_1_1BNO055" ]
    ] ],
    [ "motor", null, [
      [ "DRV8847", "classmotor_1_1DRV8847.html", "classmotor_1_1DRV8847" ],
      [ "Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "ShareMotorInfo", "classshares_1_1ShareMotorInfo.html", "classshares_1_1ShareMotorInfo" ]
    ] ],
    [ "task_control", null, [
      [ "Task_Controller", "classtask__control_1_1Task__Controller.html", "classtask__control_1_1Task__Controller" ]
    ] ],
    [ "task_data", null, [
      [ "Task_Data", "classtask__data_1_1Task__Data.html", "classtask__data_1_1Task__Data" ]
    ] ],
    [ "task_hardware", null, [
      [ "Task_Hardware", "classtask__hardware_1_1Task__Hardware.html", "classtask__hardware_1_1Task__Hardware" ]
    ] ],
    [ "Task_IMU", null, [
      [ "Task_IMU", "classTask__IMU_1_1Task__IMU.html", "classTask__IMU_1_1Task__IMU" ]
    ] ],
    [ "task_motor", null, [
      [ "Task_Motor", "classtask__motor_1_1Task__Motor.html", "classtask__motor_1_1Task__Motor" ]
    ] ],
    [ "task_TP", null, [
      [ "Task_TP", "classtask__TP_1_1Task__TP.html", "classtask__TP_1_1Task__TP" ]
    ] ],
    [ "task_User", null, [
      [ "Task_User", "classtask__User_1_1Task__User.html", "classtask__User_1_1Task__User" ]
    ] ],
    [ "task_user_v3", null, [
      [ "Task_User", "classtask__user__v3_1_1Task__User.html", "classtask__user__v3_1_1Task__User" ]
    ] ],
    [ "tp", null, [
      [ "TouchPanel", "classtp_1_1TouchPanel.html", "classtp_1_1TouchPanel" ]
    ] ]
];