var classIMU_1_1BNO055 =
[
    [ "__init__", "classIMU_1_1BNO055.html#a064deb486d419563ed16465581f17fa6", null ],
    [ "changeMode", "classIMU_1_1BNO055.html#a6e409bb6414bd49f27e5ede25a9c98ba", null ],
    [ "deint", "classIMU_1_1BNO055.html#af97b73efc965f26da9f525bbf641fd66", null ],
    [ "getCalibCoef", "classIMU_1_1BNO055.html#a00a2462bfdb3f08e5135672faf203194", null ],
    [ "getCalibStatus", "classIMU_1_1BNO055.html#a9246c1c96a19477cec5e09c84b8b37c5", null ],
    [ "readEuler", "classIMU_1_1BNO055.html#a24c48f4fa911a9560b63e4e60638b4e2", null ],
    [ "readOmega", "classIMU_1_1BNO055.html#a582c7723f7775fa3fcd6f57db42350b1", null ],
    [ "writeCalibCoef", "classIMU_1_1BNO055.html#addf8f2e39e68ea541bf11ec4609c7bc3", null ],
    [ "addr", "classIMU_1_1BNO055.html#afcc8dab24a91b8bf84d31a4b254c40e5", null ],
    [ "i2c", "classIMU_1_1BNO055.html#a6ca9d1d3616b4be3fe36516713ba6116", null ]
];